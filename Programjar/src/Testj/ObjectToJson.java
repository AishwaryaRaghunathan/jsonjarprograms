package Testj;
import java.io.FileWriter;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
// writing json
public class ObjectToJson {
		
	public static void main(String[] args) {
		
		//Gson gson= new Gson();
		Gson gson= new GsonBuilder().setPrettyPrinting().create();
		Employee ed = createmyObject();
		System.out.println(ed); // java object printed
		String j = gson.toJson(ed); // to convert java object to JSON
		System.out.println(j);
		
		
		try(FileWriter writer = new FileWriter("C:\\Users\\239978\\Desktop\\Jar File.json"))
		{
		gson.toJson(ed, writer);
		//System.out.println("ok its done go and check your myfile.json");
		}
		catch(IOException e)
		{
		   e.printStackTrace();
		  }
		}
		private static Employee createmyObject()
		{
			Employee ed= new Employee();
			ed.setName("Aishwarya");
			ed.setAge(24);
			ed.setCity("Mysore");
			return ed;
		}
}

